CFLAGS = -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion
BUILDDIR=build
SOURCEDIR=src

all: $(BUILDDIR)/bmp.o $(BUILDDIR)/image.o $(BUILDDIR)/sepia_c.o $(BUILDDIR)/main.o $(BUILDDIR)/sepia.o
	gcc -no-pie -o $(BUILDDIR)/main $^
$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c
	mkdir -p $(BUILDDIR)
	gcc -c $(CFLAGS) -o $@ $<
$(BUILDDIR)/%.o: $(SOURCEDIR)/%.asm
	mkdir -p $(BUILDDIR)
	nasm -felf64 -o $@ $<

clean:
	rm -rf $(BUILDDIR)

test:
	@echo "c:" ; \
	./$(BUILDDIR)/main example.bmp result_c.bmp c; \
	echo "nasm:" ; \
	./$(BUILDDIR)/main example.bmp result_asm.bmp a; \


.phony: clean all test