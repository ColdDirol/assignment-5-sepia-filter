#include "../include/sepia_c.h"
#include <stdint.h>

static struct pixel process_pixel(struct pixel p) {
    // tr = 0.393R + 0.769G + 0.189B
    // tg = 0.349R + 0.686G + 0.168B
    // tb = 0.272R + 0.534G + 0.131B
    static const float m[3][3] = {
            {0.393, 0.769, 0.189},
            {0.349, 0.686, 0.168},
            {0.272, 0.534, 0.131}
    };

    float tr =  m[0][0] * p.r + m[0][1] * p.g + m[0][2] * p.b;
    float tg =  m[1][0] * p.r + m[1][1] * p.g + m[1][2] * p.b;
    float tb =  m[2][0] * p.r + m[2][1] * p.g + m[2][2] * p.b;

    tr = (tr > 255 ? 255 : tr);
    tg = (tg > 255 ? 255 : tg);
    tb = (tb > 255 ? 255 : tb);

    return (struct pixel) {
            .r=(uint8_t)tr,
            .g=(uint8_t)tg,
            .b=(uint8_t)tb
    };
}

struct image sepia_c(const struct image* source) {


    struct image img = create_image(source->width, source->height);

    for (size_t x = 0; x < source->width; x++) {
        for (size_t y = 0; y < source->height; y++) {
            struct pixel p = get_pixel(source, x, y);
            set_pixel(&img, x, y, process_pixel(p));
        }
    }

    return img;
}

extern void *sepia_asm_batch(const struct pixel*, struct pixel*);

struct image sepia_asm(const struct image *source) {
    struct image img = create_image(source->width, source->height);
    size_t sz = source->width * source->height;
    size_t leftover = sz % 4;

    for (size_t i = 0; i < sz; i += 4) {
        sepia_asm_batch(source->data + i, img.data + i);
    }

    for (size_t i = 0; i < leftover; i++) {
        img.data[sz-leftover+i] = process_pixel(source->data[sz-leftover+i]);
    }

    return img;
}