#include "../include/image.h"
#include <stdint.h>

struct image create_image(size_t width, size_t height) {
    struct image img = {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };

    return img;
}

void destroy_image(struct image* img) {
    free(img->data);
}

struct pixel get_pixel(const struct image* img, size_t x, size_t y) {
    return img->data[y*img->width + x];
}

void set_pixel(struct image* img, size_t x, size_t y, const struct pixel pixel) {
    img->data[y*img->width+x] = pixel;
}