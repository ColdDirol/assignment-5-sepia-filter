%define num_of_batches 3
%define matrix_rgb_rot 0b01001001

%macro load_batch 1
    %ifnnum %1
        %error "Expected an integer argument"
    %elif %1 >= num_of_batches
        %error "Invalid batch number"
    %endif
    %if %1 = 0
        %define pattern 0b01000000
    %elif %1 = 1
        %define pattern 0b01010000
    %elif %2 = 0
        %define pattern 0b01010100
    %endif

    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2

    pinsrb xmm0, [rdi+%1*3+0], 0
    pinsrb xmm1, [rdi+%1*3+1], 0
    pinsrb xmm2, [rdi+%1*3+2], 0
    pinsrb xmm0, [rdi+%1*3+3], 4
    pinsrb xmm1, [rdi+%1*3+4], 4
    pinsrb xmm2, [rdi+%1*3+5], 4

    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    shufps xmm0, xmm0, pattern
    shufps xmm1, xmm1, pattern
    shufps xmm2, xmm2, pattern
%endmacro

%macro extract_batch 1
    %ifnnum %1
        %error "Expected an integer argument"
    %endif
    %if %1 >= num_of_batches
        %error "Invalid batch number"
    %endif

    pextrb [rsi+%1*4+0], xmm0, 0
    pextrb [rsi+%1*4+1], xmm0, 4
    pextrb [rsi+%1*4+2], xmm0, 8
    pextrb [rsi+%1*4+3], xmm0, 12
%endmacro

%macro matrix_mul 0
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    cvtps2dq xmm0, xmm0
    pminud xmm0, [max_vals]
%endmacro

%macro adjust_matrix 0
    shufps xmm3, xmm3, matrix_rgb_rot
    shufps xmm4, xmm4, matrix_rgb_rot
    shufps xmm5, xmm5, matrix_rgb_rot
%endmacro

section .rodata
    align 16
    m_row_1: dd 0.272, 0.393, 0.349, 0.272
    m_row_2: dd 0.534, 0.769, 0.686, 0.534
    m_row_3: dd 0.131, 0.189, 0.168, 0.131
    max_vals: dd 0xFF, 0xFF, 0xFF, 0xFF

section .text
global sepia_asm_batch

sepia_asm_batch:
    push rbp
    movaps xmm3, [m_row_1]
    movaps xmm4, [m_row_2]
    movaps xmm5, [m_row_3]
    %assign i 0
    %rep num_of_batches
        load_batch i
        adjust_matrix
        matrix_mul
        extract_batch i
        %assign i i+1
    %endrep
    pop rbp
    ret