#ifndef IMAGE_HEADER
#define IMAGE_HEADER

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};
#pragma pack(pop)

struct image {
    size_t width, height;
    struct pixel* data;
};

struct image create_image(size_t width, size_t height);

void destroy_image(struct image* img);

struct pixel get_pixel(const struct image* img, size_t x, size_t y);

void set_pixel(struct image* img, size_t x, size_t y, struct pixel pixel);
#endif