#ifndef TRANSFORMATIONS_HEADER
#define TRANSFORMATIONS_HEADER
#include "image.h"

struct image sepia_c(const struct image* source);
struct image sepia_asm(const struct image* source);

#endif